package devcraft.parking;


import java.time.*;
import java.time.temporal.*;

class TimeUtilities {

    static boolean isLessThanAnHour(Duration timeInParking) {
        return timeInParking.toMillis() < Duration.ofMinutes(60).toMillis();
    }

    static boolean isLessThan10Min(Duration timeInParking) {
        return timeInParking.toMillis() < Duration.ofMinutes(10).toMillis();
    }

    static long minAsMillis(int min) {
        return min * 60 * 1000L;
    }

    static Temporal max(Instant t1, Instant t2) {
        if (t1.isAfter(t2))
            return t1;
        return t2;
    }

    static Temporal min(Instant t1, Instant t2) {
        if (t1.isBefore(t2))
            return t1;
        return t2;
    }
}