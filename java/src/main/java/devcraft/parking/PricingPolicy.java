package devcraft.parking;

import java.time.*;
import java.time.temporal.*;

import static java.time.Duration.between;
import static java.time.ZoneOffset.UTC;

class PricingPolicy {
    private final boolean allowEscapeTime;

    public PricingPolicy(boolean allowEscapeTime) {
        this.allowEscapeTime = allowEscapeTime;
    }

    public int calcPayment(Instant entryTime, Instant paymentTime) {
        Duration timeInParking = between(entryTime, paymentTime);
        if (allowEscapeTime && TimeUtilities.isLessThan10Min(timeInParking))
            return 0;
        ZonedDateTime entryDate = ZonedDateTime.ofInstant(entryTime, UTC);
        ZonedDateTime next6Am = ZonedDateTime.of(entryDate.toLocalDate(), LocalTime.of(6, 0), UTC);
        if (!next6Am.isAfter(entryDate)) {
            next6Am = next6Am.plus(24, ChronoUnit.HOURS);
        }

        int payment = 0;
        while (next6Am.toInstant().isBefore(paymentTime)) {
            payment += dailyPayment(entryTime, next6Am.toInstant());
            entryTime = next6Am.toInstant();
            next6Am = next6Am.plus(24, ChronoUnit.HOURS);
        }
        payment += dailyPayment(entryTime, paymentTime);
        return payment;
    }

    private int dailyPayment(Instant entryTime, Instant paymentTime) {
        Duration timeInParking = between(entryTime, paymentTime);
        ZonedDateTime zonedEntryTime = ZonedDateTime.ofInstant(entryTime, UTC);
        boolean isWeekend = zonedEntryTime.getDayOfWeek() == DayOfWeek.THURSDAY || zonedEntryTime.getDayOfWeek() == DayOfWeek.FRIDAY || zonedEntryTime.getDayOfWeek() == DayOfWeek.SATURDAY;
        if (isWeekend) {
            // on weekend we may have day parking and night parking
            ZonedDateTime entryDay = ZonedDateTime.ofInstant(entryTime, UTC);
            ZonedDateTime nightStart = ZonedDateTime.of(entryDay.toLocalDate(), LocalTime.of(22, 0), UTC);

            Duration dayParkingDuration = Duration.between(entryTime, TimeUtilities.min(nightStart.toInstant(), paymentTime));
            Duration nightParkingDuration = Duration.between(TimeUtilities.max(entryTime, nightStart.toInstant()), paymentTime);

            int amountToPay = 0;
            if (dayParkingDuration.toMillis() > 0) {
                // calc payment for day parking
                amountToPay += 12;
                if (!TimeUtilities.isLessThanAnHour(dayParkingDuration)) {
                    long duration = dayParkingDuration.toMillis() - TimeUtilities.minAsMillis(60);
                    // calc 15min intervals
                    long intervalsToPay = 1 + duration / TimeUtilities.minAsMillis(15);

                    // add the intervals payment to total
                    amountToPay += (intervalsToPay * 3);
                }
            }
            if (nightParkingDuration.toMillis() > 0) {
                // we have some night parking
                // add payment for night time
                amountToPay += 40;
            }
            return Math.min(amountToPay, 96);
        }

        int amountToPay = 12;
        if (!TimeUtilities.isLessThanAnHour(timeInParking)) {
            long duration = timeInParking.toMillis() - TimeUtilities.minAsMillis(60);
            // calc 15min intervals
            long intervalsToPay = 1 + duration / TimeUtilities.minAsMillis(15);

            // add the intervals payment to total
            amountToPay += (intervalsToPay * 3);
        }

        // There is a maximum payment per day
        return Math.min(amountToPay, 96);
    }

}
