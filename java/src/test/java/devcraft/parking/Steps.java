package devcraft.parking;

import cucumber.api.java.en.*;
import org.junit.*;

import java.time.*;
import java.time.format.*;

public class Steps {
    private long entryTime;
    private int amountToPay;

    private boolean allowEscapeTime = true;

    @Given("^I entered the parking at (.*)$")
    public void i_entered_the_parking_at(String time) {
        entryTime = parseTime(time);
    }

    private long parseTime(String time) {
        Instant localDateTime = LocalDateTime.parse(time, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")).toInstant(ZoneOffset.UTC);
        return localDateTime.toEpochMilli();
    }

    @When("^I pay at (.*)$")
    public void i_pay_at(String time) {
        long paymentTime = parseTime(time);
        PricingPolicy policy = new PricingPolicy(allowEscapeTime);
        amountToPay = policy.calcPayment(Instant.ofEpochMilli(entryTime), Instant.ofEpochMilli(paymentTime));
    }

    @Then("^I should pay (\\d+)$")
    public void i_should_pay(int expectedAmount) {
        Assert.assertEquals(expectedAmount, amountToPay);
    }

    @Given("^no escape time$")
    public void noEscapeTime() {
        allowEscapeTime = false;
    }
}
